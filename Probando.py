import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv("/Users/javlintor/Downloads/base_datos_2008.csv.bz2")
df.dropna(inplace = True, subset = ["ArrDelay", "DepDelay", "Distance", "AirTime"])

sns.set(rc={'figure.figsize':(15,10)})
